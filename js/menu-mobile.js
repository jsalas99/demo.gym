$(document).ready(function(){
    menu_fadein = new TimelineMax({paused: true});
    menu_fadeout = new TimelineMax({paused: true});

    $('.js-menu-dropdown').click(function(){
        menu_fadein
        .set($('.js-menu-mobile'),{className:'+=is-active'})
        .to($('.js-menu-mobile'), 0.5, {left:'0%', ease: Power4.easeInOut});

        menu_fadein.play();   
    });

    $('.js-menu-dismiss').click(function(){
        menu_fadeout
        .to($('.js-menu-mobile'), 0.5, {left:'100%', ease: Power4.easeInOut}) 
        .set($('.js-menu-mobile'),{className:'-=is-active'});

        menu_fadeout.play();   
    });



    
       

    /* $('.c-dropdown-mobile__dismiss').click(function(){
        tlanim
        .to($('.c-dropdown-mobile'), 0.5, {right:'-100%', ease: Power4.easeInOut})
        .set($('.c-dropdown-mobile'),{display:'none'})  
        .set($('.c-dropdown-mobile'),{right:'-100%'});  
        tlanim.play();   
    });*/
});