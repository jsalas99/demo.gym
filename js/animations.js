$(document).ready(function(){

    //C-INTRO
    var tlc_intro_text = new TimelineMax();
    var tlc_iniciation = new TimelineMax();
    var tlc_programs = new TimelineMax();
    var tlc_join = new TimelineMax();

    var window_width = $(window).width();

        tlc_intro_text
        .to($(".c-intro__one-wrapper-texts"), 2.5, {left:'0%', autoAlpha:1, ease: Power4.easeInOut})
        .staggerTo($('.o-mask'), 0.8, {scaleX: 0, ease: Power1.easeInOut}, 0.2,'-=1.8');

        tlc_iniciation
        .to($(".c-iniciation__zero"), 2.5, {marginTop:'0px', autoAlpha:1, ease: Power4.easeInOut})
        .to($(".c-iniciation__iniciation"), 2.5, {marginTop:'0px', autoAlpha:1, ease: Power4.easeInOut},"-=2.5");

        tlc_programs
        .to($(".c-programs__card--blue"), 2.5, {top:'0px', autoAlpha:1, ease: Power4.easeInOut})
        .to($(".c-programs__card--red"), 2.5, {left:'0px', autoAlpha:1, ease: Power4.easeInOut},"-=2.5");

     
        
          
        if(window_width >= 1024){
             tlc_join
            .set($('.c-join__left-wrapper-img, .c-join__right-wrapper-img-two'), {className: "+=anim-top"})
            .set($('.c-join__right-wrapper-img-one'), {className: "+=anim-bottom"})
        }
    
        $( window ).resize(function() {
            if($(window).width() < 1024){
                $('.c-join__left-wrapper-img, .c-join__right-wrapper-img-two').removeClass('anim-top');
                $('.c-join__right-wrapper-img-one').removeClass('anim-bottom');
            }else{
                $('.c-join__left-wrapper-img, .c-join__right-wrapper-img-two').addClass('anim-top');
                $('.c-join__right-wrapper-img-one').addClass('anim-bottom');
                $('.c-join__right-wrapper-img-one, .c-join__left-wrapper-img, .c-join__right-wrapper-img-two').css('transition','none');
            }
          });

        var controller = new ScrollMagic.Controller();
        var scene_programs = new ScrollMagic.Scene({
        triggerElement: ".c-programs__title",
        triggerHook: 0.8
        })
        .setTween(tlc_programs)
        .addTo(controller);

       
        var scene_join = new ScrollMagic.Scene({
            triggerElement: ".c-join__title",
            triggerHook: 0.8,
            reverse: false
            }) 
            .setTween(tlc_join)
            .addTo(controller);


          
});