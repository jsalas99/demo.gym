$(document).ready(function(){
    $('.js-video-wrapper-button').click(function(){
        $('.js-video').get(0).play();
        $(".js-video-wrapper-button, .js-video-poster, .js-video-overlay").hide(); 
    });

    $('.js-video').on('ended',function(){
        setTimeout(function(){ 
            $(".js-video-wrapper-button, .js-video-poster, .js-video-overlay").show(); 
         }, 1000);
        
      });
});