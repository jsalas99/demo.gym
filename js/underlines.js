$(document).ready(function(){

    /* NAVBAR */

    var total_height = $('.c-nav').height() + 14+'px';
    $('.c-nav').css('height',total_height);
    var element, underline_element;
    var tl_underline = new TimelineMax({paused: true});
    var tl_underline_out = new TimelineMax({paused: true});

    $(".c-nav__link a").mouseover(function(e){
        element = '.'+$(e.target).attr('class');
        underline_element = element +' .c-nav__link-underline';
        tl_underline
        .to($(underline_element), 0.1, {height:'4px',ease: Power4.easeInOut})
        .to($(underline_element), 0.1, {width:'100%',ease: Power4.easeInOut},'-=0.05');
        tl_underline.play();
    });

    $(".c-nav__link a").mouseleave(function(e){
        tl_underline_out
        .to($(underline_element), 0.1, {width:'0%',ease: Power4.easeInOut})
        .to($(underline_element), 0.1, {height:'0px',ease: Power4.easeInOut},'-=0.05');
        tl_underline_out.play();
    });

});