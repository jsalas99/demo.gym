const path = {
    sass: './sass',
    css: './dist'
}

const gulp = require('gulp');
const sass = require('gulp-sass');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const sassLocation = path.sass + '/main.scss';
const allSassFiles = path.sass + '/**/*.scss';

gulp.task('style', () =>{
   return gulp.src(sassLocation)
        .pipe(sass())
        .pipe(gulp.dest(path.css))
})

gulp.task('watch', () => {
    return gulp.watch(allSassFiles, gulp.series('style'))
})

gulp.task('minify', () => {
    return gulp.src('js/*.js', { allowEmpty: true }) 
      .pipe(minify({noSource: true}))
      .pipe(concat('script.min.js'))
      .pipe(gulp.dest('dist'))
  })
  
  gulp.task('minify_lib', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/gsap/src/minified/TweenMax.min.js',
        'node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
        'lib/debug.addIndicators.js',
        'lib/animation.gsap.js'
    ], { allowEmpty: true })
        .pipe(minify({noSource: true}))
        .pipe(concat('lib.min.js'))
        .pipe(gulp.dest('dist'));
  });

  gulp.task('default', gulp.series(['minify']));